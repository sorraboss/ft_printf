/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ichubare <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/25 18:51:35 by ichubare          #+#    #+#             */
/*   Updated: 2016/12/18 20:49:15 by ichubare         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*link;

	link = (t_list*)malloc(sizeof(t_list));
	if (link == NULL)
		return (NULL);
	if (content == NULL)
	{
		link->content = NULL;
		link->content_size = 0;
		link->wasted = 0;
		link->repeat = 0;
	}
	else
	{
		link->content = malloc(content_size);
		link->content_size = content_size;
		ft_memmove(link->content, content, content_size);
		link->wasted = 0;
		link->repeat = 0;
	}
	link->next = NULL;
	return (link);
}
