/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fpf.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ichubare <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/18 18:27:49 by ichubare          #+#    #+#             */
/*   Updated: 2017/02/07 15:58:56 by ichubare         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ret_point(const char *str, int i, va_list v_lst, t_param_list *param)
{
	t_for_chk_l			*fcl;
	int					point;
	int					prev;
	static t_spec_list	*spec_list = NULL;

	point = i;
	fcl = ft_new_fcl();
	if (spec_list == NULL)
		spec_list = ft_new_spec_lst();
	if ((fcl->parameter = if_param(str, &point, &prev))
		&& spec_list->parameter == 0)
	{
		spec_list->parameter = what_a_param(str, point);
		va_end(v_lst);
		return (-1);
	}
	if (spec_list->parameter > 1)
	{
		while (spec_list->parameter-- > 1)
			va_arg(v_lst, void*);
	}
	while ((fcl->width = if_width(str, &point, &prev)) ||
			(fcl->precission = if_precision(str, &point)) ||
			(fcl->length = if_length(str, &point, &prev)) ||
			(if_flag(str, &point)))
	{
		if (fcl->width)
			what_the_width(str, prev, spec_list, v_lst);
		else if (fcl->precission)
		{
			what_a_precision(str, &point, spec_list, v_lst);
			fcl->if_prec = 1;
			if (spec_list->precission == 0)
				spec_list->if_prec = 1;
		}
		else if (fcl->length)
			what_the_length(str, prev, spec_list);
		else
			what_the_flag(str, spec_list, point);
	}
	if (if_type(str, &point, &prev))
		what_the_type(str, prev, spec_list);
	out_arg(spec_list, v_lst, fcl, param);
	param->count_bytes += spec_list->n_len;
	spec_list = ft_new_spec_lst();
	return (point);
}
